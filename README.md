
# RNMatrixCore

The core data functionality of matrix-js-sdk

## Installation
## Steps
## 1. Add package

 - Using Yarn `yarn add rn-chat-matrix`
 - Using NPM `npm i rn-chat-matrix`

## 2. Add peer dependencies

- Using Yarn `yarn add react-native-localize @react-native-community/async-storage node-libs-react-native react-native-randombytes`
- Using NPM `npm i react-native-localize @react-native-community/async-storage node-libs-react-native react-native-randombytes`

## 3. Add postinstall script

Add the following line to the `scripts` section of your `package.json`:
`"postinstall": "sed -i '' '$s/}/,\"browser\":{\"fs\":\"react-native-level-fs\"}}/' node_modules/olm/package.json"`

## 4. Add or edit the metro.config.js file

```
// metro.config.js
module.exports = {
    resolver: {
        extraNodeModules: require('node-libs-react-native'),
    },
}
```

## 5. Require globals and polyfill URL

Add these lines in your app before anything else: 

```
import 'node-libs-react-native/globals';
import '@rn-matrix/core/shim.js';

import {polyfillGlobal} from 'react-native/Libraries/Utilities/PolyfillFunctions';
polyfillGlobal('URL', () => require('whatwg-url').URL);
```

## 6. Install Pods
Do this in the root directory, or if you prefer run `cd ios && pod install && cd ..` in the root directory.

```
npx pod-install
```

## 7. Run postinstall script
- Using Yarn `yarn postinstall`
- Using NPM `npm run postinstall`

## 8. Proguard (Android)
Add the following line in `android/app/proguard-rules.pro`

```
-keep public class com.horcrux.svg.** {*;}
```

## 9. Initialize auth

In order to initialize the Matrix SDK to detect auth, you'll need to put this code snippet at the top level of your app, when it starts up, before your app but `after the imports we got in step #5.`

## MIT Licensed

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

- ! This is not needed if you're doing token authentication (like with SSO) - only needed for an actual auth flow 

## Usage
```
import { matrix } from 'rn-chat-matrix';
...
matrix.initAuth();

```