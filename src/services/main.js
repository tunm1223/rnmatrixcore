import chats from './chat';
import matrix from './matrix';
import messages from './message';
import users from './user';
import auth from './auth';

const debug = require('debug')('rnm:services:external.js');

class RnMatrix {
  /*************************************************
   * CLIENT METHODS
   *************************************************/

  async createClient(baseUrl, accessToken, userId, deviceId) {
    return matrix.createClient(baseUrl, accessToken, userId, deviceId);
  }

  async start(useCrypto) {
    return matrix.start(useCrypto);
  }

  async getHomeserverData(domain) {
    return matrix.getHomeserverData(domain);
  }

  getClient() {
    return matrix.getClient();
  }

  /*************************************************
   * AUTH METHODS
   *************************************************/

  initAuth() {
    return auth.init();
  }

  loginWithPassword(username, password, homeserver, initCrypto = false) {
    return auth.loginWithPassword(username, password, homeserver, initCrypto);
  }

  logout() {
    return auth.logout();
  }

  /*************************************************
   * USER METHODS
   *************************************************/

  getMyUser() {
    if (matrix.getClient()) {
      return matrix.getClient().getUser(matrix.getClient().getUserId());
    }
    // return users.getMyUser();
  }

  /*************************************************
   * ROOM METHODS
   *************************************************/

  async createRoom(options = {}) {
    const defaults = {
      visibility: 'private',
      invite: [], // list of user IDs
      room_topic: '',
    };
    return chats.createChat({ ...defaults, ...options });
  }

  async createEncryptedRoom(usersToInvite) {
    // return chats.createEncryptedChat(usersToInvite);
  }

  getRooms$(slim = false) {
    // return chats.getChats(slim);
  }

  getRoomsByType$(type) {
    // return chats.getListByType$(type);
  }

  getRoomById(roomId) {
    const roomList = this.sortRoomList(matrix.getClient() ? matrix.getClient().getVisibleRooms() : [])
    let directMessage = null;
    for (let i = 0; i < roomList.length && !directMessage; i++) {
      if (roomList[i].roomId == roomId) {
        directMessage = roomList[i]
      }
    }
    return directMessage;
  }

  joinRoom(roomIdOrAlias) {
    // chats.joinRoom(roomIdOrAlias);
  }

  leaveRoom(roomId) {
    // chats.leaveRoom(roomId);
  }

  rejectInvite(roomId) {
    // chats.leaveRoom(roomId);
  }

  sortRoomList(list) {
    return list
      .filter((r) => r.getMyMembership() === 'join')
      .sort(
        (b, a) =>
          a.timeline[a.timeline.length - 1].getTs() - b.timeline[b.timeline.length - 1].getTs()
      );
  };

  getDirectChat(userId) {
    const roomList = this.sortRoomList(matrix.getClient() ? matrix.getClient().getVisibleRooms() : [])
    let directMessage = null;
    for (let i = 0; i < roomList.length && !directMessage; i++) {
      const room = roomList[i];
      const members = room.getJoinedMembers();
      const hasUser = members.find((member) => member.userId === userId);
      if (members.length === 2 && hasUser) {
        directMessage = room;
      }
    }
    return directMessage;
  }

  setRoomName(roomId, name) {
    // const chat = chats.getChatById(roomId);
    // chat.setName(name);
  }

  async markAsRead(room) {
    await chats.markAsRead(room)
  }

  /*************************************************
   * MESSAGE METHODS
   *************************************************/

  send(content, type, roomId, eventId = null) {
    messages.send(content, type, roomId, eventId);
  }

  sendReply(roomId, relatedMessage, messageText) {
    // messages.sendReply(roomId, relatedMessage, messageText);
  }

  getMessageById(eventId, roomId, event = null) {
    // return messages.getMessageById(eventId, roomId, event);
  }

  deleteMessage(message) {
    // const { event } = message.getMatrixEvent();
    // const eventId = event.event_id;
    // const roomId = event.room_id;
    // matrix.getClient().redactEvent(roomId, eventId);
    // message.update();
  }

  editMessage(roomId, messageId, newMessageContent) {
    messages.send(newMessageContent, 'm.edit', roomId, messageId);
  }


  /*************************************************
   * User Methods
   *************************************************/

  getKnownUsers() {
    return users.getKnownUsers();
  }

  async searchUsers(searchTerm) {
    return await users.searchUsers(searchTerm);
  }

  getUserById(userId) {
    return users.getUserById(userId);
  }

  /*************************************************
   * HELPERS
   *************************************************/

  getHttpUrl(mxcUrl, width = null, height = null, resizeMethod = 'scale') {
    return matrix.getHttpUrl(mxcUrl, width, height, resizeMethod);
  }
}

const rnm = new RnMatrix();
export default rnm;
